window.onload = function() {
    prettyDate.update("2008-01-28T22:25:00Z");
  };
  QUnit.test("prettydate.format", function( assert ) {
    function test(then, expected) {
		assert.equal(prettyDate.format("2008/01/28 22:25:00", then),expected);
	}
	test("2008/01/28 22:24:30", "just now");
	test("2008/01/28 22:23:30", "1 minute ago");
	test("2008/01/28 21:23:30", "1 hour ago");
	test("2008/01/27 21:23:30", "Yesterday");
	test("2008/01/26 22:23:30", "2 days ago");
	test("2007/01/26 22:23:30", undefined);
  });
  
  /*QUnit.test("prettydate.upate", function( assert ) {
	var links = document.getElementById("qunit-fixture").getElementsByTagName("a");
	assert.equal(links[0].innerHTML, "January 27th, 2008");
	assert.equal(links[0].innerHTML, "January 28th, 2008");
	prettyDate.update("2008-01-28T22:25:00Z");
    assert.equal(links[0].innerHTML, "2 hours ago");
    assert.equal(links[2].innerHTML, "Yesterday");
  });*/